FROM alpine:3.10

ENV SOCAT_VERSION=1.7.3.3-r0

RUN apk add --no-cache socat=$SOCAT_VERSION

CMD ["/usr/bin/socat", "-h"]
